#ifndef vtk_m_Light
#define vtk_m_Light

#include <vtkm/rendering/vtkm_rendering_export.h>
#include <vtkm/Bounds.h>
#include <vtkm/Math.h>
#include <vtkm/Matrix.h>
#include <vtkm/Range.h>
#include <vtkm/Transform3D.h>
#include <vtkm/VectorAnalysis.h>
#include <vtkm/rendering/MatrixHelpers.h>

#include <memory>

namespace vtkm
{
namespace rendering
{
class VTKM_RENDERING_EXPORT Light
{
     public: 
	Light(const vtkm::Vec3f_32 position, 
	      const vtkm::Vec3f_32 lookDir, 
	      const vtkm::Vec3f_32 color, 
	      const vtkm::Float32 intensity,
	      bool isDirectional
	      ); 
	vtkm::Float32  GetIntensity() const; 
	vtkm::Vec3f_32 GetPosition() const; 
	vtkm::Vec3f_32 GetColor() const; 
	vtkm::Vec3f_32 GetLookDir(); 
	bool           IsDirectional(); 
     private: 
	struct InternalsType; 
	std::shared_ptr<InternalsType> Internals; 
};
}
}

#endif 
