//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================
#ifndef vtk_m_rendering_raytracing_TriagnleIntersector_h
#define vtk_m_rendering_raytracing_TriagnleIntersector_h

#include <vtkm/cont/DataSet.h>
#include <vtkm/rendering/raytracing/Ray.h>
#include <vtkm/rendering/raytracing/ShapeIntersector.h>
#include <vtkm/rendering/vtkm_rendering_export.h>

namespace vtkm
{
namespace rendering
{
namespace raytracing
{

class VTKM_RENDERING_EXPORT TriangleIntersector : public ShapeIntersector
{
protected:
  vtkm::cont::ArrayHandle<vtkm::Id4> Triangles;
  bool UseWaterTight;
//0TTER
  vtkm::cont::Field NormalField; 
  bool UseFieldNormals; 

public:
  TriangleIntersector();

  void SetUseWaterTight(bool useIt);

  void SetData(const vtkm::cont::CoordinateSystem& coords,
               vtkm::cont::ArrayHandle<vtkm::Id4> triangles);

  vtkm::cont::ArrayHandle<vtkm::Id4> GetTriangles();
  vtkm::Id GetNumberOfShapes() const override;


  VTKM_CONT void IntersectRays(Ray<vtkm::Float32>& rays, bool returnCellIndex = false) override;
  VTKM_CONT void IntersectRays(Ray<vtkm::Float64>& rays, bool returnCellIndex = false) override;

// add a new parameter for field that has a default parameter
  VTKM_CONT void IntersectionData(Ray<vtkm::Float32>& rays,
                                  const vtkm::cont::Field scalarField,
                                  const vtkm::Range& scalarRange = vtkm::Range()) override;

  VTKM_CONT void IntersectionData(Ray<vtkm::Float64>& rays,
                                  const vtkm::cont::Field scalarField,
                                  const vtkm::Range& scalarRange = vtkm::Range()) override;

  //0TTER 
  /*
  VTKM_CONT void IntersectionData(Ray<vtkm::Float32>& rays,
                                  const vtkm::cont::Field scalarField,
                                  const vtkm::cont::Field normalField,
                                  const vtkm::Range& scalarRange = vtkm::Range()) override;

  VTKM_CONT void IntersectionData(Ray<vtkm::Float64>& rays,
                                  const vtkm::cont::Field scalarField,
                                  const vtkm::cont::Field normalField,
				  const vtkm::Range& scalarRange = vtkm::Range()) override;
 */
  template <typename Precision>
  VTKM_CONT void IntersectRaysImp(Ray<Precision>& rays, bool returnCellIndex);

  template <typename Precision>
  VTKM_CONT void IntersectionDataImp(Ray<Precision>& rays,
                                     const vtkm::cont::Field scalarField,
                                     const vtkm::Range& scalarRange);

  //0TTER
  /*
  template <typename Precision>
  VTKM_CONT void IntersectionDataImp(Ray<Precision>& rays,
                                     const vtkm::cont::Field scalarField,
                                     const vtkm::Range& scalarRange,
                                     const vtkm::cont::Field normalField);
				     */ 

  void SetFieldNormals(const vtkm::cont::Field normalField) override;
}; // class intersector
}
}
} //namespace vtkm::rendering::raytracing
#endif //vtk_m_rendering_raytracing_TriagnleIntersector_h
