#ifndef vtk_m_shader
#define vtk_m_shader

#include <memory>
#include <vector>

#include <vtkm/cont/DataSet.h>

#include <vtkm/rendering/raytracing/Camera.h>
#include <vtkm/rendering/raytracing/TriangleIntersector.h>
#include <vtkm/rendering/Light.h>

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <vtkm/cont/ArrayHandleUniformPointCoordinates.h>
#include <vtkm/cont/ColorTable.h>
#include <vtkm/cont/Timer.h>



namespace vtkm
{
namespace rendering
{
class Shader{
public:
  void (*ShaderFunction)(vtkm::Vec3f_32& pixelCol , vtkm::rendering::Shader);
  vtkm::cont::ArrayHandle<vtkm::Float32> FloatProperties;
  void BindFloats(std::vector<vtkm::Float32> floatProps);
  void BindFunction(void (*func)(vtkm::Vec3f_32& , vtkm::rendering::Shader));
}; //end of shader
}
}
#endif
