#include <vtkm/rendering/Shader.h>

#include <memory>
#include <vector>

#include <vtkm/cont/DataSet.h>

#include <vtkm/rendering/raytracing/Camera.h>
#include <vtkm/rendering/raytracing/TriangleIntersector.h>
#include <vtkm/rendering/Light.h>
#include <vtkm/rendering/raytracing/RayTracer.h>

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <vtkm/cont/ArrayHandleUniformPointCoordinates.h>
#include <vtkm/cont/ColorTable.h>
#include <vtkm/cont/Timer.h>

#include <vtkm/rendering/raytracing/Logger.h>
#include <vtkm/rendering/raytracing/RayTracingTypeDefs.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/WorkletMapField.h>


namespace vtkm
{
namespace rendering
{
void Shader::BindFloats(std::vector<vtkm::Float32> floatProps){
  FloatProperties = vtkm::cont::make_ArrayHandle(floatProps);
}

void Shader::BindFunction(void (*func)(vtkm::Vec3f_32&, vtkm::rendering::Shader)){
  ShaderFunction = func;
}
}
}
