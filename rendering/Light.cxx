#include <vtkm/rendering/Light.h> 

#include <vtkm/Assert.h> 
#include <vtkm/cont/TryExecute.h> 

namespace vtkm
{
namespace rendering
{
struct Light::InternalsType
{
   vtkm::Vec3f_32 Position;
   vtkm::Vec3f_32 LookDir;
   vtkm::Vec3f_32 Color;
   vtkm::Float32 Intensity; 
   bool IsDirectional;

   VTKM_CONT 
   InternalsType(const vtkm::Vec3f_32 position,
                 const vtkm::Vec3f_32 lookDir,
                 const vtkm::Vec3f_32 color,
                 const vtkm::Float32 intensity,
		 bool isDirectional) 
      :Position(position)
      ,LookDir(lookDir)
      ,Color(color)
      ,Intensity(intensity)
      ,IsDirectional(isDirectional)
      {
      }
};

Light::Light(const vtkm::Vec3f_32 position,
             const vtkm::Vec3f_32 lookDir,
             const vtkm::Vec3f_32 color,
             const vtkm::Float32  intensity,
	     bool                 isDirectional)
	: Internals(new InternalsType(position, lookDir, color, intensity, isDirectional))	
	{
		std::cout << "initializing?" << std::endl; 
		std::cout << this -> Internals -> Position << std::endl;
	        std::cout << position << std::endl << std::endl; 

		std::cout << this -> Internals -> Color << std::endl;
	        std::cout << color << std::endl << std::endl; 

		std::cout << this -> Internals -> LookDir << std::endl;
	        std::cout << lookDir << std::endl << std::endl; 
	}

vtkm::Float32 Light::GetIntensity() const{
	return this->Internals->Intensity;
	}
vtkm::Vec3f_32 Light::GetPosition() const{
	return this->Internals->Position;
	}
vtkm::Vec3f_32 Light::GetColor() const{
	return this->Internals->Color;
	}
vtkm::Vec3f_32 Light::GetLookDir(){
	return this->Internals->LookDir;
	}
bool Light::IsDirectional(){
	return this->Internals->IsDirectional;
	}
}
}
