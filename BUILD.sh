#USE VTK-M version 1.6.0 so download & unzip
cd ../
wget https://gitlab.kitware.com/vtk/vtk-m/-/archive/v1.6.0/vtk-m-v1.6.0.zip
unzip vtk-m-v1.6.0.zip

#Replace original rendering directory with the one we wrote
rm -r vtk-m-v1.6.0/vtkm/rendering
cp -r vtkmShader/rendering vtk-m-v1.6.0/vtkm/

#Update examples to include the example where we show what 
#has been updated in rendeirng dir
rm vtk-m-v1.6.0/examples/CMakeLists.txt
cp vtkmShader/examples/CMakeLists.txt vtk-m-v1.6.0/examples/CMakeLists.txt
cp -r vtkmShader/examples/lighting_exp vtk-m-v1.6.0/examples/

#now build vtkm 
cd vtk-m-v1.6.0
mkdir build
cd build
cmake ../ \
  -DBUILD_TESTING=OFF \
  -DVTKm_ENABLE_EXAMPLES=ON\
  -DVTKm_USE_64BIT_IDS=ON

make -j3 LightingExp

#need to copy necessary files to their appropriate locations 
cp -r ~/vtkmShader/examples/lighting_exp/data ~/vtk-m-v1.6.0/build/examples/lighting_exp/

#run a test to make sure everything is peachy 
cd examples/lighting_exp/
./LightingExp
xdg-open PBR.ppm
